<?php

/**
 * Post Types
 *
 * This file registers any custom post types
 */



/**
 * Create new CPT - Floor Plans
 */

register_via_cpt_core( array(
	    __( 'Floor Plan', 'thegardens' ), // Singular
	    __( 'Floor Plans', 'thegardens' ), // Plural
	    'floor_plan' // Registered name/slug
	),
	array( 
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'show_ui' 			 => true,
        'show_in_menu' 		 => true,
        'show_in_nav_menus'  => true,
		'exclude_from_search' => false,
		'rewrite' => array('slug'=> 'homes/floor-plans' ),
		//'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' , 'genesis-cpt-archives-settings' )
		'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
		)
 );
 
 $partner_categories = array(
    __( 'Plan Category', 'thegardens' ), // Singular
    __( 'Plan Categories', 'thegardens' ), // Plural
    'plan_cat' // Registered name
);

// Will register an 'Actress' Taxonomy to 'movies' post-type
register_via_taxonomy_core( $partner_categories, 
	array(
		'rewrite' => array('slug'=> 'floor-plan' )
	), 
	array( 'floor_plan' ) 
	);