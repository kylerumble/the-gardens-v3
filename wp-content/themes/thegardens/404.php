<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package thegardens
 */

get_header(); ?>

	<div class="row">

		<div class="primary content-area small-12<?php echo ( is_active_sidebar( 'primary' ) ) ? ' large-8' : '';?> columns">
			
			<main id="main" class="site-main" role="main">

			<article class="page">
			
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'thegardens' ); ?></h1>
					</header><!-- .page-header -->
	
					<div class="page-content">
						<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'thegardens' ); ?></p>
	
	
					</div><!-- .page-content -->
				</section><!-- .error-404 -->
			
			</article>

		</main><!-- #main -->
		
		</div><!-- .primary -->
		
	</div><!-- .wrap -->

<?php get_footer(); ?>
