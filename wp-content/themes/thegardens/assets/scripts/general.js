/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT / GPLv2 License.
*/
(function(w){
	
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	var ua = navigator.userAgent;
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && /OS [1-5]_[0-9_]* like Mac OS X/i.test(ua) && ua.indexOf( "AppleWebKit" ) > -1 ) ){
		return;
	}

    var doc = w.document;

    if( !doc.querySelector ){ return; }

    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;

    if( !meta ){ return; }

    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true;
    }

    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false;
    }
	
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
				
		// If portrait orientation and in one of the danger zones
        if( (!w.orientation || w.orientation === 180) && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){
				disableZoom();
			}        	
        }
		else if( !enabled ){
			restoreZoom();
        }
    }
	
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );

})( this );


(function($) {
	
	'use strict';
	
	// responsive videos
	var $all_oembed_videos = $("iframe[src*='youtube'], iframe[src*='vimeo']");
	
	$all_oembed_videos.each(function() {
	
		var _this = $(this);
				
		if (_this.parent('.embed-container').length === 0) {
		  _this.wrap('<div class="embed-container"></div>');
		}
		
		_this.removeAttr('height').removeAttr('width');
 	
 	});
	
	// open PDF's in new tab
	
	$('a[href$=".pdf"]').prop('target', '_blank');
	
	
	//$('*[data-toggle="popover"]').popover();
	
	$(".map").popover({
          placement: "top",
          html: "true",
          trigger: "hover",
          content: $('#location-map').html()
    });
	
	// floor plan menu
	function hide_cat_menu() {
		$('.dropdown-toggle').removeClass('open');
	}
	 
	// Dropdown toggle
	$('.dropdown-toggle').click(function(){
	
	  $(this).toggleClass('open');
	});
	
	
	$(document).click(function(e) {
	  var target = e.target;
	  if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle')) {
		//$('.dropdown').hide();
		hide_cat_menu();
	  }
	});
	
	
	
	// Call anyhting that needs a resize check
	var updateWindow = _.debounce(function(e) {
		hide_cat_menu();
	}, 300); // Maximum run of once per 300 milliseconds
	
	// Add the event listener
	window.addEventListener("resize", updateWindow, false);
	window.addEventListener("load", updateWindow, false);
	
	
	$('.home .slideshow').append('<span class="next-section"><i class="fa fa-chevron-down fa-2x"></i></span>');
	
	/*$('.home .slideshow')
	.find('.section-wrapper').append('<span class="next-section"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-chevron-down fa-stack-1x "></i></span></span>');*/
	
	var  before_header =  $('.before-wrapper'),
	 	  site_header =  $('.site-wrapper'),
		  top_offset = 0;
	
	$(document).on('click', '.next-section', function(){
	  				
		if( $('.sticky-wrapper .stuck').css('position') === 'fixed' ) {
			top_offset = top_offset + before_header.height() + site_header.height();
		}
			
		// fixed wp admin bar?
		if( $('body').hasClass('admin-bar') && $('#wpadminbar').css('position') === 'fixed' ) {
			top_offset = top_offset + $('#wpadminbar').height();
		}
	  
	  $.smoothScroll({
				offset: top_offset * -1,
				scrollTarget: $('.primary')
			});
	});
	
	
})(jQuery);

