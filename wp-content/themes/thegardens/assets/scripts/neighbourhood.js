(function($) {
	
	'use strict';	
	
	var w, h, parentOffset, offset, x, y;
  
  	$('#map-box span').on("click", function(e) {
							
		w = $(this).width();
		h = $(this).height();
		
		offset = 10;
					
		if( w && h ) {

			parentOffset = $(this).offset();
			x = ((e.pageX - parentOffset.left - offset ) /w) * 100;
			y = ((e.pageY - parentOffset.top - offset ) /h) * 100;
							
			$('#mouse-xy').val(x + "/" + y);
			
		}
		
		
  });
  
  
  $(window).on('resize', function(){
      // reset map
	  $('.map-key a').not(this).removeClass( 'active' );
	  $('.map-key ul').hide();
  });

  
  
  $(".accordion h3").click(function () {
        var cat = $(this);
		show_category( cat );
    });
  
	// highlight the location on tooltip hover
	
	$('.tooltip-element').each(function() {
		$(this).mouseenter(function() {
			var cat = $(this).data( 'cat' );
			if( !$('#' + cat ).next('ul').is(':visible') ) {
				$('#' + cat ).trigger('click');
			}
		});
	});
	
	
	function show_category( cat ) {
		var parent = cat.parent();
		//cat.siblings().not($parent.next()).removeClass('next');
		//cat.siblings("ul:visible").slideUp(100);
		parent.find('ul').not(cat.next('ul')).slideUp('fast');
		cat.next("ul").slideToggle(100);
        cat.toggleClass("current");
        cat.siblings("h3").removeClass("current");
	}
	
	
	$(".tooltip-element").mouseenter(function () {
        $(".map-key a").removeClass( 'active' );
		var location = $(this).data( 'location' );
		$('#location-' + location ).addClass('active');
    });
	
	$(".map-key a").hover(function () {
        $(this).addClass('active');
		$(".map-key a").not(this).removeClass( 'active' );
    });
	
	
})(jQuery);


