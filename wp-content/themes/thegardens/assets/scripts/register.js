(function($) {
	
	'use strict';
	
	var error;
	function validateForm() {
		// clean up
		$("input, select").removeClass("error-field");
		$("label").removeClass("label-error");
		
		// reset errors
		error = false;
		
		// 	validate
	
		$('*[data-validate="required"]').each(function(i, obj) {			
			if ( $(this).val().length === 0 ) {
				$(this).addClass("error-field");
				error = true;
			}
		});
		
	
		var field=document.forms["register-form"]["Emails[Primary]"];
		var atpos=field.value.indexOf("@");
		var dotpos=field.value.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=field.value.length){
			//alert("Please provide a valid e-mail address");
			$(field).addClass("error-field");
			error = true;
		}
		
		var field=document.forms["register-form"]["Questions[37400]"];
		if ($(field).is(":checked") == false) {
			$(field).parent().find("label").addClass("label-error");
			error = true;
		}
		if (parseInt($('#math-input').attr('sizez')) !== parseInt($('#math-input').val())) {
			$('label[for="math-input"').addClass('label-error');
			error = true;
		}
		if (error === true) {
			$(".error-field").first().focus();
			return false;
		}
		
	}
	
	var x = Math.floor(Math.random() * 10);
	var y = Math.floor(Math.random() * 10);
	var z = x+y;
	
	var math_question = "<span>" + x + " + " + y + " = </span>";
	
	var math_question_label = $('label[for="math-input"]').text();
	
	
	$('label[for="math-input"]').html( math_question_label + math_question );
	$('#math-input').attr('sizez', z );
	
	
	$("#register-form").submit(function(){
         return validateForm();
    });
		
	
	
})(jQuery);

