<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package thegardens
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="row">

			<div class="site-info small-12 columns">
				<ul>
				<li class="register"><?php printf('<a href="%s">%s</a>', get_permalink(11), __( 'Register', 'thegardens' ) );?></li>
				<li><?php printf( '<a href="#" class="map" data-toggle="popover">%s</a>', __( 'Now Selling. Visit us Today.', 'thegardens' ));?></li>
				<li>604-271-3331</li>
				<li><a href="mailto:<?php echo antispambot('thegardens@townline.ca');?>"><?php _e('Email', 'thegardens' );?></a></li>
				<li class="divider"></li>
				<li><?php printf('<a href="http://www.townline.ca" target="_blank"><img src="%s" alt="%s"/></a>', CHILD_THEME_IMG .'/townline-logo.png', __( 'Townline', 'thegardens' ) );?></li>
				
				<li><?php printf('<a href="http://www.macmarketingsolutions.com" target="_blank"><img src="%s" alt="%s"/></a>', CHILD_THEME_IMG .'/mac-grey-logo.png', __( 'MAC Marketing Solutions', 'thegardens' ) );?></li>
				</ul>

			</div><!-- .site-info -->
			
			<div class="disclaimer small-12 columns">
				<p><?php echo _e('This is not an offering for sale. Any such offering can only be made with  a disclosure statement. E.&amp;O.E.', 'thegardens');?></p>
			</div>

		</div><!-- .row -->
		
	</footer><!-- #colophon -->
	
	<?php 
	// default map
	//printf('<div id="location-map" style="display: none"><img src="%s/map.png" width="246" height="205"></div>', CHILD_THEME_IMG );
	// holiday map
	printf('<div id="location-map" style="display: none"><img src="%s/map-holiday.png" width="246" height="285"></div>', CHILD_THEME_IMG );
	?>
	
</div><!-- #page -->
<?php wp_footer(); ?>
<!-- W3TC-include-js-head -->
<div id="lasso-code" style="display:none;">
<!-- LASSO -->
<?php if (strpos(strtolower(get_the_title()), "register") === FALSE) { ?>
    <script type="text/javascript">
    var _ldstJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
    _ldstJsHost += "www.mylasso.com";
    document.write(unescape("%3Cscript src='" + _ldstJsHost + "/analytics.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    <!--
    var LassoCRM = LassoCRM || {};
    (function(ns){
        ns.tracker = new LassoAnalytics('LAS-132994-05');
    })(LassoCRM);
    try {
        LassoCRM.tracker.setTrackingDomain(_ldstJsHost);
        LassoCRM.tracker.init();
        LassoCRM.tracker.track();
    } catch(error) {}
    -->
    </script>
<?php }; ?>

<?php
// only on Chinese site
if( 'zh-hans' == ICL_LANGUAGE_CODE ) {
	?>
	<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1530688170564990');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1530688170564990&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
	<?php
}
?>
</div>
</body>
</html>
