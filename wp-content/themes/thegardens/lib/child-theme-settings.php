<?php

// Add excerpts to pages
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}
//add_action( 'init', 'my_add_excerpts_to_pages' );

// Add additional image sizes
add_image_size( 'full-width-photo', 1900, 99999 );
add_image_size( 'slide', 1900, 99999 );
add_image_size( 'design-size', 720, 432, true ); 
add_image_size( 'map', 890, 9999 ); 
add_image_size( 'gallery-thumbnail', 400, 400, true ); 

add_image_size( 'plan-thumb', 170, 9999 ); 

// Add custom image sizes to media dropdown
function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'front-page-category-thumbnail' => __( 'Front Page Category thumbnail' ),
    ) );
}
//add_filter( 'image_size_names_choose', 'my_custom_sizes' );