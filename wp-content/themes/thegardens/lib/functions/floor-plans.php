<?php

// redirect to first term
function floor_plan_first_term_redirect() {
    $cpt = 'floor_plan';
	if (is_post_type_archive($cpt)) {
		$tax = 'plan_cat';
		$args = array();
		$cats = get_terms($tax, $args);
		if (count($cats)) {
		   wp_redirect(get_term_link(reset($cats), $tax));
		   exit();
		}
		else {
			$site_url = function_exists( 'icl_get_home_url' ) ? icl_get_home_url() : site_url();
			wp_redirect( $site_url );
			exit();	
		}
	}
} // function wpse_135306_redirect
add_action('template_redirect', 'floor_plan_first_term_redirect');



function get_total_sqft() {
	global $post;
	
	$rows = get_field('sqft');
	if($rows) {
		$total = 0;
		
		foreach($rows as $row) {
			$total += $row['amount'];
		}
	
		return $total;
	}	
	
	return 0;
}


function get_sqft() {
	global $post;
	
	$rows = get_field('sqft');
	
	if($rows) {
		$total = 0;
		$out = '';
		
		foreach($rows as $row) {
			$total += $row['amount'];
			$out .= sprintf( '<tr><th>%s</th><td>%s SQ FT</td></tr>', $row['type'], $row['amount'] );
		}
		
		$out .= sprintf( '<tr><th>%s</th><td>%s SQ FT</td></tr>', __( 'Total', 'thegardens' ), $total );
	
		return sprintf( '<div class="sqft-chart"><table>%s</table></div>', $out );
	}	
	
	return 0;
}