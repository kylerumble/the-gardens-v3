<?php
// Foobox Gallery

add_image_size( 'foobox-thumbnail', 400, 400, true ); // 1140 x 100 upload @ 2280 x 369
add_image_size( 'foobox-large', 1200, 999999 ); // 1140 x 100 upload @ 2280 x 369
add_image_size( 'foobox-mobile', 600, 999999 ); // 1140 x 100 upload @ 2280 x 369

function foobox_gallery( $additional_classes = '', $thumbnail_size = 'foobox-thumbnail', $default_size = 'foobox-large', $mobile_size = 'foobox-mobile' ) {
	
	static $gallery_num;
	
	$gallery_num++;
	
	$photos = get_field('gallery');
										
	if( empty( $photos ) )
		return FALSE;
		
	$size = $default_size;
															
	if( wp_is_mobile() ) {
		$size = $mobile_size;
	}
					
    $out = '';
	
	$out .= '<div class="gallery">';
    
	$out .= sprintf('<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-3 gallery %s">', $additional_classes );
	
	foreach( $photos as $photo ):
    	
		$thm = $photo['thumbnail'];
		$thumb = wp_get_attachment_image( $thm['ID'], 'foobox-thumbnail', '', array( 'class' => '' ) );
		
		$img = $photo['image'];
		$img_attr = wp_get_attachment_image_src( $img['ID'], $size ); // returns an array
		
		$out .= '<li class="gallery-item">';
		$out .= sprintf('<a href="%s" title="%s" class="foobox" rel="foobox-%s">', $img_attr[0], esc_html( $thm['caption'] ), $gallery_num );
		
		$out .= sprintf('<div class="lazyload" ><!--%s--></div></a>', $thumb );
        $out .= '</li>';           
	
	endforeach;
	
	
	
    $out .= '</ul></div>';
	
	return $out;
}