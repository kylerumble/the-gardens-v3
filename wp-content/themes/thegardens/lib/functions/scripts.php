<?php

// Load theme stylesheet
function load_theme_stylesheet() {
	
	wp_enqueue_style( CHILD_THEME_NAME, get_stylesheet_uri(), false, CHILD_THEME_VERSION );
	
}
add_action( 'wp_enqueue_scripts', 'load_theme_stylesheet' );


// Load Scripts
function load_scripts() {
    	
	// Modernizr
	wp_enqueue_script( 'modernizr', CHILD_THEME_JS . '/modernizr.custom.js', FALSE, NULL );
	 
	//wp_enqueue_script( 'add-to-any', '//static.addtoany.com/menu/page.js', FALSE, NULL, TRUE );
	
	wp_enqueue_script( 'wp-util' );
		
	wp_enqueue_script( 'masked-input', CHILD_THEME_JS . '/jquery.maskedinput.js', FALSE, NULL, TRUE );
	
	// Royal Slider
	wp_enqueue_script( 'royal-slider', CHILD_THEME_JS . '/jquery.royalslider.custom.min.js', array( 'jquery' ), NULL, TRUE );
	wp_enqueue_script( 'royalslider-init' , CHILD_THEME_JS . '/royalslider.js', array( 'jquery', 'royal-slider'), NULL, TRUE );
	
	
	wp_enqueue_script( 'tooltip' , CHILD_THEME_JS . '/tooltip.js', array( 'jquery'), NULL, TRUE );
	wp_enqueue_script( 'popover' , CHILD_THEME_JS . '/popover.js', array( 'jquery', 'tooltip'), NULL, TRUE );
	
	// Neighbourhood
	wp_enqueue_script( 'neighbourhood', CHILD_THEME_JS . '/neighbourhood.js', array('jquery'), NULL, TRUE );
	
	wp_enqueue_script( 'smooth-scroll', CHILD_THEME_JS . '/jquery.smooth-scroll.min.js', array('jquery'), NULL, TRUE );
	
	
	// Register
	wp_enqueue_script( 'register' , CHILD_THEME_JS . '/register.js', array(), NULL, TRUE );
	
	// Gallery
	//wp_enqueue_script( 'images-loaded', CHILD_THEME_JS . '/imagesloaded.pkgd.js', FALSE, NULL, TRUE );
	//wp_enqueue_script( 'isotope', CHILD_THEME_JS . '/isotope.pkgd.js', FALSE, NULL, TRUE );
	//wp_enqueue_script( 'packery-mode', CHILD_THEME_JS . '/packery-mode.pkgd.js', FALSE, NULL, TRUE );
	wp_enqueue_script( 'lazyload-any', CHILD_THEME_JS . '/jquery.lazyload-any.min.js', array('jquery'), NULL, TRUE );
	wp_enqueue_script( 'gallery' , CHILD_THEME_JS . '/gallery.js', array(
			'jquery', 
			//'isotope', 
			'tooltip', 
			'popover',
			'lazyload-any',
			'smooth-scroll'
			 ), NULL, TRUE );
	
	// Child Theme JS
	wp_enqueue_script( CHILD_THEME_NAME , CHILD_THEME_JS . '/general.js', array('jquery', 'wp-util'), NULL, TRUE );
	
	/*
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	*/
	
}
add_action( 'wp_enqueue_scripts', 'load_scripts' );