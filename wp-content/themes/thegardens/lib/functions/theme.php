<?php
add_filter( 'be_social_widget_items', function($items) {
	
	$show_language_switcher = get_field( 'show_language_switcher', 'options' );
	
	if( !$show_language_switcher )
		return $items;
	
	if (function_exists('icl_get_languages') ) {
    	$languages = icl_get_languages('skip_missing=0');
 
       	if(!empty($languages)){
			foreach($languages as $l){
				if(!$l['active']){
					$lang_name = $l['language_code'] == 'zh-hans' ? '中文' : 'EN';
					$items .= sprintf( '<li class="lang"><a href="%s">%s</a></li>', $l['url'], $lang_name );
                }
           }
       }
    }
	return $items;
}, 10, 1 );

// split content in 1/2
function columns_half( $content ) {
		// Split the content
		$content_parts = preg_split('/<span id="more-\d+"><\/span>/i', $content);
		
		// Grab first part of content and apply filters
		$result['intro'] = wpautop( array_shift( $content_parts) );
		
		// Content has a more part?
		if(!empty($content_parts)):
			$result['more'] = wpautop( implode($content_parts) );
			$content =  sprintf( '<div class="row"><div class="small-12 large-6 columns">%s</div><div class="small-12 large-6 columns">%s</div></div>', $result['intro'], $result['more'] );
		endif;
		
		return $content;
	}


function custom_columns_half( $content ) {
		// Split the content
		$content_parts = preg_split('/<!--more-->/i', $content);
		
		// Grab first part of content and apply filters
		$result['intro'] = wpautop( array_shift( $content_parts) );
		
		// Content has a more part?
		if(!empty($content_parts)):
			$result['more'] = wpautop( implode($content_parts) );
			$content =  sprintf( '<div class="row"><div class="small-12 large-6 columns">%s</div><div class="small-12 large-6 columns">%s</div></div>', $result['intro'], $result['more'] );
		endif;
		
		return $content;
	}

/**
 * Custom Body Class
 *
 * @param array $classes
 * @return array
 */
add_filter( 'body_class', 'kr_body_class' );
function kr_body_class( $classes ) {
  $classes[] = ICL_LANGUAGE_CODE;
  return $classes;
}


function kr_slideshow() {
	
	$slides = get_field( 'slides' );
	
	
	if( empty( $slides )  ) {
		return FALSE;		
	}
	
	$size = 'slide';
	
	if( function_exists( 'wpmd_is_device' ) ) {
		
		if( wpmd_is_device() ) {
			$size = 'large';
		}
		
		if( wpmd_is_phone() ) {
			$size = 'medium';
		}
	}
	
	?>
	   
	<div class="slideshow">
    
            <div id="slider" class="royalSlider rsCustom">
              
            <?php 
				
			foreach( $slides as $slide ): 
												
				$img_attr = wp_get_attachment_image_src( $slide['photo']['ID'], $size ); // returns an array
				$caption = '';
				$content = $slide['content'];
				if( $content ) {
					$caption = sprintf('<div class="rsCaption"><div class="row"><div class="small-12 columns">%s</div></div></div>', $content );
				}
				
				printf('<div><a data-rsw="%s" data-rsh="%s" class="rsImg" href="%s"></a>%s</div>', $img_attr[1], $img_attr[2], $img_attr[0], $caption );
	
			endforeach;
		
			
			?>
            
            </div>
                                    
    </div>
  
    <?php	
	
}


function kr_masonry_gallery() {
	
	$size = 'full';
	
	if( function_exists( 'wpmd_is_device' ) ) {
		if( wpmd_is_phone() ) {
			$size = 'medium';
		}
	}
	
	?>
	
	<div class="gallery-grid">
		<?php while (have_rows('gallery')) : the_row(); ?>
		<?php 
		$thm = get_sub_field('thumbnail'); 
		$thm_attr = wp_get_attachment_image_src( $thm['ID'], $size ); // returns an array
				
		$img = get_sub_field('image');
		$img_attr = wp_get_attachment_image_src( $img['ID'], $size ); // returns an array
		?>
		<div class="gallery-img-wrapper <?php the_sub_field('size'); ?>">
			<a href="<?php echo $img_attr[0]; ?>" title="<?php echo $thm['caption']; ?>" class="foobox" rel="gallery"><img src="<?php echo $thm_attr[0]; ?>" alt="<?php echo $thm['alt']; ?>" /></a>
		</div>
		<?php endwhile; ?>
	</div>
	
	<?php
}

add_filter('the_title', 'thegardens_alternate_title');

function thegardens_alternate_title( $title ) {
	global $post;
	
	if( !in_the_loop() )
		return $title;
	
	if( $at = get_post_meta( $post->ID, 'alternate_page_title', true ) ) {
		return 	$at;
	}
	
	return $title;
}
