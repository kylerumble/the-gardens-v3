<?php
get_header(); ?>

	<div class="wrap">

		<div class="primary content-area">
			<main id="main" class="site-main" role="main">
			<?php if ( have_posts() ) : ?>
				
				<?php /* Start the Loop */ ?>
				
				<?php while ( have_posts() ) : the_post(); ?>
					
					<div class="row">
					
					<div class="small-12 large-9 columns">
					<?php
						// Calla logo
						print( '<h2 class="calla"><span>Calla</span> Dahlia</h2>' );
						printf( '<h1 class="entry-title">%s %s</h1>', __( 'Plan', 'thegardens' ), get_the_title() );
						
						the_post_thumbnail( 'large' );
					?>
					</div>
					<div class="small-12 large-3 columns sidebar">
					<?php
						// description
						$description = get_post_meta( get_the_ID(), 'description', true );
						if( $description )
							printf( '<h3>%s</h3>', $description );
						
						// sqft table
						echo get_sqft();
						
						// download
						$download = get_field( 'download' );
						if( $download )
							printf( '<p><a href="%s" class="button" target="blank">%s</a></p>', $download, __( 'Download Plan', 'thegardens' ) );
						
						printf( '<p><a href="%s" class="button">%s</a></p>', get_post_type_archive_link( 'floor_plan' ), __( 'Back To All Plans', 'thegardens' ) );
						
						// Levels
						$photos = get_field( 'photos' );
						
						if( $photos ) {
							
							echo '<div class="levels">';
							
							$count = 1;
							
							foreach( $photos as $photo ) {
								
								$attachment_id = $photo['ID'];
								
								$thumbnail = wp_get_attachment_image( $attachment_id, 'medium' );
								
								printf( '<h5>%s: %s</h5>%s', __( 'Level', 'thegardens' ), $count, $thumbnail );
								
								$count++;
							}
							
							echo '</div>';
						}
						
					?>
					</div>
					
					</div>
				<?php endwhile; ?>

				
			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

<?php get_footer(); ?>
