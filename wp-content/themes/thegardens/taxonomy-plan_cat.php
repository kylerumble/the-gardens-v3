<?php
get_header(); ?>

	<div class="wrap">

		<div class="primary content-area">
			<main id="main" class="site-main" role="main">
			<?php if ( have_posts() ) : ?>
				
				<?php
				$term = is_tax() ? 
						get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ) : 
						$wp_query->get_queried_object();
	
				$current_term = $term ? $term->slug : '';
				// menu
				$tax = 'plan_cat';
				$args = array();
				$terms = get_terms($tax, $args);
				if ( count( $terms ) ) {
					$out = '';
					foreach( $terms as $term ) {
						$current = $term->slug == $current_term ? ' class="current-menu-item"' : '';
						$out .= sprintf( '<li%s><a href="%s">%s</a></li>', $current, get_term_link( $term, $tax ), $term->name );
					}
				  	
					printf( '<div class="floor-cats"><div class="row"><div class="small-12 columns text-center"><h4 class="dropdown-title dropdown-toggle">%s</h4><div class="dropdown"><ul>%s</ul></div></div></div></div>', __( 'Categories', 'thegardens' ), $out );
				}
				
				?>
				
				<?php /* Start the Loop */ ?>
				<div class="row"><div class="small-12 columns">
				<?php
				// Calla logo
				print( '<h2 class="calla"><span>Calla</span> Dahlia</h2>' );
				// Single Cat Title
				printf( '<h2 class="cat-title">%s</h2>', single_term_title( '', false ) );
				
				printf( '<ul %s>', 'class="small-block-grid-1 medium-block-grid-2 large-block-grid-3 plans"' );
				?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						// Title 
						$title = the_title( '<h3>', '</h3>', FALSE );
						// total sqft
						$sqft  = get_total_sqft() ? sprintf( '<p>%s SQ FT</p>', get_total_sqft() ) : '' ;
						// details
						$details = $title . $sqft;
						// get floor plan thumbnail
						$thumbnail = get_the_post_thumbnail( get_the_ID(), 'plan-thumb' );
						
						printf( '<li><a href="%s">%s<div>%s</div></a></li>',get_permalink(),$thumbnail,  $details );
					?>

				<?php endwhile; ?>

				<?php
				echo '</ul>';
				?>
				</div></div>
			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

<?php get_footer(); ?>
