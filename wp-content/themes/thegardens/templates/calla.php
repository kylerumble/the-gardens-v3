<?php
/*
Template Name: Calla
*/
 

get_header(); ?>
	
	<?php
	
	if( has_post_thumbnail() ) {
		
		$size = 'full-width-photo';
	
		if( function_exists( 'wpmd_is_device' ) ) {
			
			if( wpmd_is_device() ) {
				$size = 'large';
			}
			
			if( wpmd_is_phone() ) {
				$size = 'medium';
			}
		}
		
		printf('<div class="full-width-photo">%s</div>', get_the_post_thumbnail( get_the_ID(), 'full-width-photo' ) );	
	}
	?>
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'templates/parts/content', 'page' ); ?>
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .row -->
	
	<?php
	$blocks = get_post_meta( $post->ID, 'blocks', true );
	$ids  = get_acf_post_ids( $blocks );
	
	if( $blocks ) {
		$ids  = get_acf_post_ids( $blocks );
		if( $ids ) {
			// arguments, adjust as needed
			$args = array(
				'post_type'      => 'page',
				'posts_per_page' => -11,
				'post_status'    => 'publish',
				'post__in'=> $ids,
				'order' => 'post__in'
			);
		
			$loop = new WP_Query( $args );
		
			if ( $loop->have_posts() ) : 
				
				while ( $loop->have_posts() ) : $loop->the_post(); 
		
					global $post;
					
					if( has_post_thumbnail( $loop->post->ID) ) {
		
						$size = 'full-width-photo';
					
						if( function_exists( 'wpmd_is_device' ) ) {
							
							if( wpmd_is_device() ) {
								$size = 'large';
							}
							
							if( wpmd_is_phone() ) {
								$size = 'medium';
							}
						}
						
						printf('<div class="full-width-photo margin-top">%s</div>', get_the_post_thumbnail( $loop->post->ID, 'full-width-photo' ) );	
					}
					
					print( '<div class="row">

						<div class="primary content-area small-12 columns">
							
							<main id="main" class="site-main" role="main">' );
									
						//get_template_part( 'templates/parts/content', 'page' );
					
					
						?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header class="entry-header">
								<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							</header><!-- .entry-header -->
						
							<div class="entry-content">
								<?php 
									$content = apply_filters( 'the_content', $post->post_content );
									echo custom_columns_half( $content );
								?>
								<?php
									wp_link_pages( array(
										'before' => '<div class="page-links">' . __( 'Pages:', 'thegardens' ),
										'after'  => '</div>',
									) );
								?>
							</div><!-- .entry-content -->
						
							<footer class="entry-footer">
								<?php edit_post_link( __( 'Edit', 'thegardens' ), '<span class="edit-link">', '</span>' ); ?>
							</footer><!-- .entry-footer -->
						</article><!-- #post-## -->

						<?php
					
					print( '</main><!-- #main -->
							</div><!-- .primary -->
					
						</div><!-- .row -->' );
		
				endwhile;
				
			endif;
		
			wp_reset_postdata();
		}
	}
	?>
	
	
<?php get_footer(); ?>