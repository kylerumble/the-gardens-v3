<?php
/*
Template Name: Contact
*/
 

get_header(); ?>
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					
					<div class="row">
					
						<div class="small-12 large-4 columns">
							<header class="entry-header">
								<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							</header><!-- .entry-header -->
							<div class="entry-content">
							<?php
							the_content();
							?>
							</div>
						</div>
					
						<div class="small-12 large-8 columns">
							<?php echo do_shortcode( '[wpgmza id="2"]' ); ?>
						</div><!-- .entry-content -->
					
					<div>
				
					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'thegardens' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .row -->
	
	
	<?php
	if( has_post_thumbnail() ) {
		
		$size = 'full-width-photo';
	
		if( function_exists( 'wpmd_is_device' ) ) {
			
			if( wpmd_is_device() ) {
				$size = 'large';
			}
			
			if( wpmd_is_phone() ) {
				$size = 'medium';
			}
		}
		
		printf('<div class="full-width-photo">%s</div>', get_the_post_thumbnail( get_the_ID(), 'full-width-photo' ) );	
	}
	?>
	
<?php get_footer(); ?>