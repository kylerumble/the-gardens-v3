<?php
/*
Template Name: Homes Interior
*/
 

get_header(); ?>
	
	<?php
	
	if( has_post_thumbnail() ) {
		
		$size = 'full-width-photo';
	
		if( function_exists( 'wpmd_is_device' ) ) {
			
			if( wpmd_is_device() ) {
				$size = 'large';
			}
			
			if( wpmd_is_phone() ) {
				$size = 'medium';
			}
		}
		
		printf('<div class="full-width-photo">%s</div>', get_the_post_thumbnail( get_the_ID(), 'full-width-photo' ) );	
	}
	?>
	
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					add_filter( 'the_content', 'columns_half', 99 );
					?>
					
					<?php get_template_part( 'templates/parts/content', 'page' ); ?>
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .row -->

	
<?php get_footer(); ?>