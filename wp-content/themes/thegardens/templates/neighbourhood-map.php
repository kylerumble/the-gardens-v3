<?php
/*
Template Name: Neighbourhood Map
*/


get_header(); ?>
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'templates/parts/content', 'page' ); ?>
	
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->
		
		<div class="secondary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

					<?php
					include locate_template('./templates/partials/map.php', false, false);
					?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .row -->
	
	
<?php get_footer(); ?>