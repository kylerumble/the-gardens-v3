<?php
/*
Template Name: Neighbourhood
*/


get_header(); ?>
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'templates/parts/content', 'page' ); ?>
	
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->
		
		<div id="map" class="secondary content-area small-12 columns">
		
			<?php
				printf( '<h2 class="section-title">%s</h2>', __( 'Map', 'thegardens' ) );
			
				include locate_template('./templates/partials/map.php', false, false);
			?>

		</div><!-- .primary -->
		
		<?php
		$gallery = foobox_gallery();
		
		if( $gallery ):
		?>
		
		<div id="gallery" class="secondary content-area small-12 columns">

			<?php
				printf( '<h2 class="section-title">%s</h2>', __( 'Gallery', 'thegardens' ) );
				
				echo $gallery;
			?>

		</div><!-- .primary -->
		
		<?php
		endif;
		?>

	</div><!-- .row -->
	
	
<?php get_footer(); ?>