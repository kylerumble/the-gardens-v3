<?php

function the_coordinates() {
	if( current_user_can('level_10') )
		print('<div class="mouse-xy"><div class="row"><div class="small-12 columns"><label>Click on map, then copy map coordinates.</label><input id="mouse-xy" type="text" placeholder="Map Coordinates"></div></div></div>');
}


function get_map_box() {
	
	$result = '';
	
	if ( has_post_thumbnail() ) {
	
		$size = 'map';
		
		$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(), $size );
				
		$width = $image_src[1];
		$height = $image_src[2];
		
		$locations = '';
			
		$rows = get_field('locations');
						
		if( !empty( $rows ) ):
						
			foreach( $rows as $key => $row):
			
				list($x, $y) = explode('/', $row['coordinates']);
				
				$offset_x = $row['offset_x'] ? $row['offset_x'] : 0;
				$offset_y = $row['offset_y'] ? $row['offset_y'] : -2;
				
				//$x = $x + ($offset_x/$width) . '%';
				//$y = $y + ($offset_y/$height) . '%';
				
				$x = $x . '%';
				$y = $y . '%';
							
				$xy = sprintf("top:%s; left:%s;", $y, $x );
				
				$cat = $row['skin'];
				
				$position = sprintf("position: '%s'" , $row['position'] );
				$skin = sprintf("skin: '%s'" , $row['skin'] );
				
				$title = $row['title'];
				
				//$options = json_encode( array( 'position' => $position ) );
				$locations .= sprintf('<span id="tooltip-element-%s" class="tooltip-element" style="%s" data-location="%s" data-cat="%s" data-tipped-options="%s,%s, offset: { x: %s, y: %s }" title="%s"></span>', $key, $xy, $key, $cat, $position, $skin, $offset_x, $offset_y, $title);
	
			endforeach;
		endif;
	
		$result = sprintf( '<div id="map-top"></div><div id="map-box"><span>%s%s</span></div>',  get_the_post_thumbnail( get_the_ID(), $size, array( 'class' => 'location-map') ) , $locations );
		
	}
	
	return $result;
	
}


function get_map_key() {
		
	$result = '';
	
	global $post;
	
	$cat_titles = array(
					
		'shops-services' => 'Shops & Services',
		'recreation' => 'recreation',
		'schools' => 'Schools'
					
	);
	
	$locations = array();
		
	$rows = get_field('locations');			
					
	if( empty( $rows ) )
		return;
	
	// organize by category				
	foreach( $rows as $key => $row):
		$locations[$row['skin']][$key] = array( $row['title'], $row['description'] );
	endforeach;
		
	$i = 0;
	
	$result .= '<div class="map-key accordion">';
					
	// spit out two categories per column
	foreach( $cat_titles as $cat_id => $cat_title ) {	
		if( isset( $locations[$cat_id] ) && !empty( $locations[$cat_id] ) ):
			$result .= sprintf('<h3 id="%s" class="%s">%s</h3>', $cat_id, $cat_id, $cat_title );
			$result .= sprintf('<ul class="%s">', $cat_id );
			foreach( $locations[$cat_id] as $key => $val ):
				++$i;
				$result .= sprintf('<li><a class="location-anchor" id="location-%s" href="#tooltip-element-%s"><i><strong>%s</strong></i> <strong>%s</strong><span>%s</span></a></li>', $key, $key, $i, $val[0], $val[1] );
			endforeach;
			$result .= '</ul>';
		endif;
	}
	
	$result .= '</div>';
	
	return $result;
}


function the_map_grid() {
	
	$map = get_map_box();
	$map_key = get_map_key();
		
	$pdf = get_field('pdf');	
	if( $pdf ) {
		$pdf = sprintf( '<div class="download"><p><a href="%s" target="_blank">%s</a></p></div>', $pdf, __( 'Download PDF', 'camellia' ) );
	}
	
	if( $map  )
		printf( '<div class="show-for-xxlarge-up"><div class="row"><div class="small-12 xxlarge-9 columns">%s</div><div class="small-12 xxlarge-3 columns">%s%s</div></div></div>', $map, $map_key, $pdf );
		
	printf('<div class="hide-for-xxlarge-up"><img style="display: block;" src="%s/neighbourhood/map-mobile.png" />%s</div>', CHILD_THEME_IMG, $pdf );	
	
}

// Output the map 

the_coordinates();

the_map_grid();