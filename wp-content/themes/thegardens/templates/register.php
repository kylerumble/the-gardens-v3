<?php
/*
Template Name: Register
*/
 

get_header(); ?>
	
	<?php
	if( has_post_thumbnail() ) {
		
		$size = 'full-width-photo';
	
		if( function_exists( 'wpmd_is_device' ) ) {
			
			if( wpmd_is_device() ) {
				$size = 'large';
			}
			
			if( wpmd_is_phone() ) {
				$size = 'medium';
			}
		}
		
		printf('<div class="full-width-photo">%s</div>', get_the_post_thumbnail( get_the_ID(), 'full-width-photo' ) );	
	}
	?>
	
	<?php _e('', 'thegardens' );?>
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->
					
					<div class="row">
					<div class="entry-content small-12 large-6 columns">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
					</div>
					
					<form id="register-form" action="http://www.mylasso.com/registrant_signup.php" method="post">
					<input type="hidden" name="domainAccountId" value="LAS-132994-05" />
					<input type="hidden" name="guid" value="" />
					<input type="hidden" name="LassoUID" value="ZXT&uV5#LV" />
					<input type="hidden" name="ClientID" value="19" />
					<input type="hidden" name="ProjectID" value="791" />
					<input type="hidden" name="SignupThankyouLink" value="<?php echo get_permalink(116); ?>" />
					
					<div class="row">
					
						<div class="small-12 large-6 columns">
							
							<div class="q-container">
								<label for="FirstName"><?php _e( 'First Name', 'thegardens' );?> *</label>
								<input type="text" id="FirstName" name="FirstName" data-validate="required" />
							</div>
							<div class="q-container">
								<label for="LastName"><?php _e('Last Name', 'thegardens' );?> *</label>
								<input type="text" id="LastName" name="LastName" data-validate="required" />
							</div>
							<div class="q-container">
								<label for="Email"><?php _e('Email', 'thegardens' );?> *</label>
								<input type="text" id="Email" name="Emails[Primary]" data-validate="required" />
							</div>
							<div class="q-container">
								<label for="Phones[Home]"><?php _e('Phone', 'thegardens' );?> *</label>
								<input name="Phones[Home] " type="text" id="Phones[Home]" class="phone" data-validate="required" />
							</div>
							<div class="q-container">
								<label for="PostalCode"><?php _e('Postal Code', 'thegardens' );?> *</label>
								<input name="PostalCode" type="text" id="PostalCode" data-validate="required" />
							</div>
							
						</div>
					
						<div class="small-12 large-6 columns">
							<div class="q-container">
								<label><?php _e('How did you hear about The Gardens?', 'thegardens' );?> *</label>
								<div class="select-box">
								
								
		
									<select name="Questions[37398]" id="Questions[37398]" data-validate="required">
										<option value=""><?php _e('Choose one...', 'thegardens' );?></option>
										<?php
										$options = array(
											173827 =>  __( 'Print Ads', 'thegardens' ),
											173828 =>  __( 'Radio/TV', 'thegardens' ),
											173829 =>  __( 'Signage/Drive By', 'thegardens' ),
											173830 =>  __( 'Word of Mouth', 'thegardens' ),
											173831 =>  __( 'Web Search', 'thegardens' ),
											173832 =>  __( 'Direct Mail', 'thegardens' ),
											173833 =>  __( 'Social Media', 'thegardens' ),
											173834 =>  __( 'Realtor', 'thegardens' ),
											173835 =>  __( 'Other', 'thegardens' )
						
										);
										
										foreach( $options as $key => $val ) {
											printf('<option value="%s">%s</option>', $key, $val );
										}
										?>
									</select>
								</div>
							</div>		
							
							
							<div class="q-container">
								<label><?php _e('What is your desired type of home?', 'thegardens' );?> *</label>
								<div class="select-box">
								
								
		
									<select name="Questions[11449]" id="Questions[11449]" data-validate="required">
										<option value=""><?php _e('Choose one...', 'thegardens' );?></option>
										<?php
										$options = array(
											52798 =>  __( '1 Bedroom', 'thegardens' ),
											52799 =>  __( '1 Bedroom + Den', 'thegardens' ),
											52800 =>  __( '2 Bedroom', 'thegardens' ),
											52801 =>  __( '2 Bedroom + Den', 'thegardens' ),
											52802 =>  __( '3 Bedroom', 'thegardens' ),
											191158 =>  __( 'Townhome', 'thegardens' )
						
										);
										
										foreach( $options as $key => $val ) {
											printf('<option value="%s">%s</option>', $key, $val );
										}
										?>
									</select>
								</div>
							</div>				
							
												
							<div class="q-container">
								<label><?php _e('', 'thegardens' );?>Are you a realtor?*</label>
								<div class="select-box">						
									<select name="Questions[11445]" id="Questions[11445]" data-validate="required">
										<option value=""><?php _e('Choose one...', 'thegardens' );?></option>
										<option value="52775"><?php _e('Yes', 'thegardens' );?></option>
										<option value="52776"><?php _e('No', 'thegardens' );?></option>
									</select>
								</div>
							</div>
							<div class="q-container">
								<label><?php _e('Are you working with a realtor?', 'thegardens' );?> *</label>
								<div class="select-box">
									<select name="Questions[11444]" id="Questions[11444]" data-validate="required">
										<option value=""><?php _e('Choose one...', 'thegardens' );?></option>
										<option value="52773" /><?php _e('Yes', 'thegardens' );?></option>
										<option value="52774" /><?php _e('No', 'thegardens' );?></option>
									</select>
								</div>
							</div>
							<div class="q-container">
								<label><?php _e('', 'thegardens' );?>Are you looking to</label>
								<div class="select-box">
									<select name="Questions[37094]" id="Questions[37094]">
										<option value=""><?php _e('Choose one...', 'thegardens' );?></option>
										<option value="172871"><?php _e('Own', 'thegardens' );?></option>
										<option value="172872"><?php _e('Invest', 'thegardens' );?></option>
									</select>
								</div>
							</div>
						  
							<div class="q-container">
								<label for="comments"><?php _e('Comments', 'thegardens' );?></label>
								<textarea id="Comments" name="Comments"></textarea>
							</div>
							<div class="q-container">
								<div id="consent-wrapper" class="radio-wrapper">
									<label><?php _e('Confirm your interest in becoming a Townline VIP?', 'thegardens' );?> *</label>
									<div class="radio-btns-wrapper">
										<input name="Questions[37400]" value="173836" id="consent-yes" type="checkbox" class="css-checkbox" checked /><label for="consent-yes" class="css-label">Yes</label>
									</div>
									<div id="disclaimer"><?php _e('By clicking “YES”and “SUBMIT”, you consent to Townline Homes Inc. and its current and future affiliates and companies including MAC Marketing Solutions sending you email messages regarding current and future products and services, event invitations, newsletters, announcements, promotions, and other publications. You may unsubscribe at any time.', 'thegardens' ); ?></div>
								</div>
							</div>
							<div class="q-container math-container">
								<label for="math-input"><?php _e( 'Please answer this question *: ', 'thegardens' );?></label>
								<input type="text" id="math-input" value="" />
							</div>
							<div class="q-container submit-container">
								<p>* <?php _e('Fields marked with an asterisk are required.', 'thegardens' );?></p>
								<div id="submit-wrapper">
									<input type="submit" value="<?php _e( 'Submit', 'thegardens' );?>" class="button" id="submit-btn" />
								</div>
							</div>
							
					
					</div>
					
					</div>
					
					</form>
	
				</article><!-- #post-## -->
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .row -->
	

<script type="text/javascript">
var _ldstJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
_ldstJsHost += "www.mylasso.com";
document.write(unescape("%3Cscript src='" + _ldstJsHost + "/analytics.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
<!--
try {
var tracker = new LassoAnalytics('LAS-132994-05');
tracker.setTrackingDomain(_ldstJsHost);
tracker.init();
tracker.track();
} catch(error) {}
-->
</script>

<script type="text/javascript">
(function(i) {var u =navigator.userAgent;
var e=/*@cc_on!@*/false; var st = setTimeout;if(/webkit/i.test(u)){st(function(){var dr=document.readyState;
if(dr=="loaded"||dr=="complete"){i()}else{st(arguments.callee,10);}},10);}
else if((/mozilla/i.test(u)&&!/(compati)/.test(u)) || (/opera/i.test(u))){
document.addEventListener("DOMContentLoaded",i,false); } else if(e){     (
function(){var t=document.createElement("doc:rdy");try{t.doScroll("left");
i();t=null;}catch(e){st(arguments.callee,0);}})();}else{window.onload=i;}})
(function() {document.forms[0].guid.value = tracker.readCookie("ut");});
</script>


<?php get_footer(); ?>