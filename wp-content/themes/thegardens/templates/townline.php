<?php
/*
Template Name: Townline
*/
 

get_header(); ?>
	
	<?php
	if( has_post_thumbnail() ) {
		
		$size = 'full-width-photo';
	
		if( function_exists( 'wpmd_is_device' ) ) {
			
			if( wpmd_is_device() ) {
				$size = 'large';
			}
			
			if( wpmd_is_phone() ) {
				$size = 'medium';
			}
		}
		
		printf('<div class="full-width-photo">%s</div>', get_the_post_thumbnail( get_the_ID(), 'full-width-photo' ) );	
	}
	?>
	
	<div class="row">

		<div class="primary content-area small-12 columns">
			
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->
					
					<div class="row">
					
						<div class="small-12 large-6 columns">
							<?php
							$video_url = 'https://vimeo.com/50261232?autoplay=1';
							$video_thumbnail = sprintf('<img src="%s" alt="%s"/>', CHILD_THEME_IMG .'/townline-video.jpg', __( 'Townline video', 'thegardens' ) );
							printf('<a href="%s" class="foobox">%s</a>', $video_url, $video_thumbnail );
							?>
						</div>
					
						<div class="entry-content small-12 large-6 columns">
							<?php the_content(); ?>
						</div><!-- .entry-content -->
					
					<div>
				
					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'thegardens' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->
					
				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .row -->
	
	
	
	
<?php get_footer(); ?>